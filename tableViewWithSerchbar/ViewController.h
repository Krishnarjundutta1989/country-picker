//
//  ViewController.h
//  tableViewWithSerchbar
//
//  Created by click labs 115 on 10/5/15.
//  Copyright (c) 2015 cli. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "showViewController.h"
@interface ViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,UISearchDisplayDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tbldata;


@end

