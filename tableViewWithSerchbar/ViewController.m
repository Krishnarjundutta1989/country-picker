//
//  ViewController.m
//  tableViewWithSerchbar
//
//  Created by click labs 115 on 10/5/15.
//  Copyright (c) 2015 cli. All rights reserved.
//

#import "ViewController.h"




@interface ViewController (){
    NSArray *arrayWithTblData;
    NSArray *codes;
    NSMutableArray *countryNames;
    NSMutableArray *serchTableData;
    NSMutableArray *filterTblData;
    NSString *strCell ;
    NSString *strShowDataInDestination;
    NSMutableArray *countryImage;
    
}
@property (strong, nonatomic) IBOutlet UISearchBar *tblSearch;
//@property (strong, nonatomic) IBOutlet UITableView *tbldata;
@property (strong, nonatomic) IBOutlet UIImageView *tblImage;

@end

@implementation ViewController
@synthesize tbldata;
@synthesize tblSearch;

- (void)viewDidLoad {
    [super viewDidLoad];
    arrayWithTblData = [NSArray new];
    codes = [[NSArray alloc] initWithArray:[NSLocale ISOCountryCodes]];
    countryNames =[NSMutableArray arrayWithObjects:@"Afghanistan",@"African Union",@"Albania",@"Algeria",@"American Samoa",@"Andorra",@"Angola",@"Anguilla",@"Antarctica",@"Antigua & Barbuda",@"Arab League",@"Argentina",@"Armenia",@"Aruba",@"ASEAN",@"Australia",@"Austria",@"Azerbaijan",@"Bahamas",@"Bahrain",@"Bangladesh",@"Barbados",@"Belarus",@"Belgium",@"Belize",@"Benin",@"Bermuda",@"Bhutan",@"Bolivia",@"Bosnia & Herzegovina",@"Botswana",@"Brazil",@"Brunei",@"Bulgaria",@"Burkina Faso",@"Burundi",@"Cambodja",@"Cameroon",@"Canada",@"Cape Verde",@"CARICOM",@"Cayman Islands",@"Central African Republic",@"Chad",@"Chile",@"China",@"CIS",@"Colombia",@"Commonwealth",@"Comoros",@"Congo-Brazzaville",@"Congo-Kinshasa(Zaire)",@"Cook Islands",@"Costa Rica",@"Cote d'Ivoire",@"Croatia",@"Cuba",@"Cyprus",@"Czech Republic",@"Denmark",@"Djibouti",@"Dominica",@"Dominican Republic",@"Ecuador",@"Egypt",@"El Salvador",@"England",@"Equatorial Guinea",@"Eritrea",@"Estonia",@"Ethiopia",@"European Union",@"Faroes",@"Fiji",@"Finland",@"France",@"Gabon",@"Gambia",@"Georgia",@"Germany",@"Ghana",@"Gibraltar",@"Greece",@"Greenland",@"Grenada",@"Guadeloupe",@"Guam",@"Guatemala",@"Guernsey",@"Guinea",@"Guinea-Bissau",@"Guyana",@"Haiti",@"Honduras",@"Hong Kong",@"Hungary",@"Iceland",@"India",@"Indonezia",@"Iran",@"Iraq",@"Ireland",@"Islamic Conference",@"Isle of Man",@"Israel",@"Italy",@"Jamaica",@"Japan",@"Jersey",@"Jordan",@"Kazakhstan",@"Kenya",@"Kiribati",@"Kosovo",@"Kuwait",@"Kyrgyzstan",@"Laos",@"Latvia",@"Lebanon",@"Lesotho",@"Liberia",@"Libya",@"Liechtenshein",@"Lithuania",@"Luxembourg",@"Macao",@"Macedonia",@"Madagascar",@"Malawi",@"Malaysia",@"Maldives",@"Mali",@"Malta",@"Marshall Islands",@"Martinique",@"Mauritania",@"Mauritius",@"Mexico",@"Micronesia",@"Moldova",@"Monaco",@"Mongolia",@"Montenegro",@"Montserrat",@"Morocco",@"Mozambique",@"Myanmar(Burma)",@"Namibia",@"NATO",@"Nauru",@"Nepal",@"Netherlands",@"Netherlands Antilles",@"Nicaragua",@"Niger",@"Nigeria",@"Netherlands",@"New Caledonia",@"New Zealand",@"North Korea",@"Northern Cyprus",@"Northern Ireland",@"Norway",@"Oman",@"Olimpic Movement",@"OPEC",@"Pakistan",@"Palau",@"Palestine",@"Panama",@"Papua New Guinea",@"Paraguay",@"Peru",@"Philippines",@"Poland",@"Portugal",@"Puerto Rico",@"Qatar",@"Red Cross",@"Reunion",@"Romania",@"Russian Federation",@"Rwanda",@"Saint Lucia",@"Samoa",@"San Marino",@"Sao Tome & Principe",@"Saudi Arabia",@"Scotland",@"Senegal",@"Serbia(Yugoslavia)",@"Seychelles",@"Sierra Leone",@"Singapore",@"Slovakia",@"Slovenia",@"Solomon Islands",@"Somalia",@"Somaliland",@"South Africa",@"South Korea",@"Spain",@"Sri Lanka",@"St Kitts & Nevis",@"St Vincent & the Grenadines",@"Sudan",@"Suriname",@"Swaziland",@"Sweden",@"Switzerland",@"Syria",@"Tahiti(French Polinesia)",@"Taiwan",@"Tajikistan",@"Tanzania",@"Thailand",@"Timor-Leste",@"Togo",@"Tonga",@"Trinidad & Tobago",@"Tunisia",@"Turkey",@"Turkmenistan",@"Turks and Caicos Islands",@"Tuvalu",@"Uganda",@"Ukraine",@"United Arab Emirates",@"United Kingdom(Great Britain)",@"United Nations",@"United States of America(USA)",@"Uruguay",@"Uzbekistan",@"Vanutau",@"Vatican City",@"Venezuela",@"Viet Nam",@"Virgin Islands British",@"Virgin Islands US",@"Wales",@"Western Sahara",@"Yemen",@"Zambia",@"Zimbabwe",nil];
    
    countryImage = [NSMutableArray arrayWithObjects:@"Afghanistan.png",@"African Union.png",@"Albania.png",@"Algeria.png",@"American Samoa.png",@"Andorra.png",@"Angola.png",@"Anguilla.png",@"Antarctica.png",@"Antigua & Barbuda.png",@"Arab League.png",@"Argentina.png",@"Armenia.png",@"Aruba.png",@"ASEAN.png",@"Australia.png",@"Austria.png",@"Azerbaijan.png",@"Bahamas.png",@"Bahrain.png",@"Bangladesh.png",@"Barbados.png",@"Belarus.png",@"Belgium.png",@"Belize.png",@"Benin.png",@"Bermuda.png",@"Bhutan.png",@"Bolivia.png",@"Bosnia & Herzegovina.png",@"Botswana.png",@"Brazil.png",@"Brunei.png",@"Bulgaria.png",@"Burkina Faso.png",@"Burundi.png",@"Cambodja.png",@"Cameroon.png",@"Canada.png",@"Cape Verde.png",@"CARICOM.png",@"Cayman Islands.png",@"Central African Republic.png",@"Chad.png",@"Chile.png",@"China.png",@"CIS.png",@"Colombia.png",@"Commonwealth.png",@"Comoros.png",@"Congo-Brazzaville.png",@"Congo-Kinshasa(Zaire).png",@"Cook Islands.png",@"Costa Rica.png",@"Cote d'Ivoire.png",@"Croatia.png",@"Cuba.png",@"Cyprus.png",@"Czech Republic.png",@"Denmark.png",@"Djibouti.png",@"Dominica.png",@"Dominican Republic.png",@"Ecuador.png",@"Egypt.png",@"El Salvador.png",@"England.png",@"Equatorial Guinea.png",@"Eritrea.png",@"Estonia.png",@"Ethiopia.png",@"European Union.png",@"Faroes.png",@"Fiji.png",@"Finland.png",@"France.png",@"Gabon.png",@"Gambia.png",@"Georgia.png",@"Germany.png",@"Ghana.png",@"Gibraltar.png",@"Greece.png",@"Greenland.png",@"Grenada.png",@"Guadeloupe.png",@"Guam.png",@"Guatemala.png",@"Guernsey.png",@"Guinea.png",@"Guinea-Bissau.png",@"Guyana.png",@"Haiti.png",@"Honduras.png",@"Hong Kong.png",@"Hungary.png",@"Iceland.png",@"India.png",@"Indonezia.png",@"Iran.png",@"Iraq.png",@"Ireland.png",@"Islamic Conference.png",@"Isle of Man.png",@"Israel.png",@"Italy.png",@"Jamaica.png",@"Japan.png",@"Jersey.png",@"Jordan.png",@"Kazakhstan.png",@"Kenya.png",@"Kiribati.png",@"Kosovo.png",@"Kuwait.png",@"Kyrgyzstan.png",@"Laos.png",@"Latvia.png",@"Lebanon.png",@"Lesotho.png",@"Liberia.png",@"Libya.png",@"Liechtenshein.png",@"Lithuania.png",@"Luxembourg.png",@"Macao.png",@"Macedonia.png",@"Madagascar.png",@"Malawi.png",@"Malaysia.png",@"Maldives.png",@"Mali.png",@"Malta.png",@"Marshall Islands.png",@"Martinique.png",@"Mauritania.png",@"Mauritius.png",@"Mexico.png",@"Micronesia.png",@"Moldova.png",@"Monaco.png",@"Mongolia.png",@"Montenegro.png",@"Montserrat.png",@"Morocco.png",@"Mozambique.png",@"Myanmar(Burma).png",@"Namibia.png",@"NATO.png",@"Nauru.png",@"Nepal.png",@"Netherlands.png",@"Netherlands Antilles.png",@"Nicaragua.png",@"Niger.png",@"Nigeria.png",@"Netherlands.png",@"New Caledonia.png",@"New Zealand.png",@"North Korea.png",@"Northern Cyprus.png",@"Northern Ireland.png",@"Norway.png",@"Oman.png",@"Olimpic Movement.png",@"OPEC.png",@"Pakistan.png",@"Palau.png",@"Palestine.png",@"Panama.png",@"Papua New Guinea.png",@"Paraguay.png",@"Peru.png",@"Philippines.png",@"Poland.png",@"Portugal.png",@"Puerto Rico.png",@"Qatar.png",@"Red Cross.png",@"Reunion.png",@"Romania.png",@"Russian Federation.png",@"Rwanda.png",@"Saint Lucia.png",@"Samoa.png",@"San Marino.png",@"Sao Tome & Principe.png",@"Saudi Arabia.png",@"Scotland.png",@"Senegal.png",@"Serbia(Yugoslavia).png",@"Seychelles.png",@"Sierra Leone.png",@"Singapore.png",@"Slovakia.png",@"Slovenia.png",@"Solomon Islands.png",@"Somalia.png",@"Somaliland.png",@"South Africa.png",@"South Korea.png",@"Spain.png",@"Sri Lanka.png",@"St Kitts & Nevis.png",@"St Vincent & the Grenadines.png",@"Sudan.png",@"Suriname.png",@"Swaziland.png",@"Sweden.png",@"Switzerland.png",@"Syria.png",@"Tahiti(French Polinesia).png",@"Taiwan.png",@"Tajikistan.png",@"Tanzania.png",@"Thailand.png",@"Timor-Leste.png",@"Togo.png",@"Tonga.png",@"Trinidad & Tobago.png",@"Tunisia.png",@"Turkey.png",@"Turkmenistan.png",@"Turks and Caicos Islands.png",@"Tuvalu.png",@"Uganda.png",@"Ukraine.png",@"United Arab Emirates.png",@"United Kingdom(Great Britain).png",@"United Nations.png",@"United States of America(USA).png",@"Uruguay.png",@"Uzbekistan.png",@"Vanutau.png",@"Vatican City.png",@"Venezuela.png",@"Viet Nam.png",@"Virgin Islands British.png",@"Virgin Islands US.png",@"Wales.png",@"Western Sahara.png",@"Yemen.png",@"Zambia.png",@"Zimbabwe.png",nil];
    serchTableData = [NSMutableArray new];
    filterTblData = [NSMutableArray new];
    filterTblData = [NSMutableArray arrayWithArray:countryNames];
    //serchTableData = [NSMutableArray arrayWithArray:countryNames];
    // Do any additional setup after loading the view, typically from a nib.
}
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  filterTblData.count;
    //return  serchTableData.count;
    
    
}
-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reuse" ];
    cell.textLabel.text = filterTblData [indexPath.row];
    //  cell.textLabel.text = serchTableData [indexPath.row];
    return cell;
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row %2) {
        [cell setBackgroundColor:[UIColor lightGrayColor]] ;
        
    }
    else{
        
        [cell setBackgroundColor:[UIColor grayColor]] ;
    }
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText   // called when text changes (including clear)
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF BEGINSWITH[cd] %@", searchBar.text];
    NSArray *list = [countryNames filteredArrayUsingPredicate:predicate];
    
    if (searchBar.text.length == 0){
        filterTblData = [NSMutableArray arrayWithArray:countryNames];
        
    }
    else if (searchBar.text.length >= 1){
        filterTblData = [NSMutableArray new];
        
        filterTblData = [NSMutableArray arrayWithArray:list];
        
        
        
    }
    [tbldata reloadData];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"goDestination"] ) {
       
            NSIndexPath *indexPath = [tbldata indexPathForSelectedRow];
            showViewController *destViewController = segue.destinationViewController;
            destViewController.strShowDataInDestination = [countryNames objectAtIndex:indexPath.row];
        }
    
       // displayViewController *dvc = (displayViewController *) [segue destinationViewController];
        
        //NSInteger passData = [[NSString stringWithFormat:@"%@",strCell]integerValue];
        
        //showViewController *destinationVC = (showViewController *) [segue destinationViewController];
       // destinationVC.strShowDataInDestination = strCell;
    }

/*
- (void) tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    //UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    strCell = [NSString new];
    strCell = [countryNames objectAtIndex:(indexPath.row)];
}*/


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end